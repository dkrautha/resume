# yaml-language-server: $schema=https://raw.githubusercontent.com/dkrautha/cv.typ/main/cv.typ.schema.json

personal:
  name: David Krauthamer
  email: dkrautha@pm.me
  phone: (+1) 508 560 4027
  url: https://davidkrauthamer.com
  location:
    city: Hoboken
    region: New Jersey
    postalCode: 07030
    country: United States of America
  profiles:
    - network: LinkedIn
      username: David Krauthamer
      url: https://linkedin.com/in/dkrautha

work:
  - organization: Stevens Institute of Technology & Circle Optics
    url: https://circleoptics.com/
    location: Hoboken, New Jersey
    positions:
    - position: "ECE Department Student Assistant"
      startDate: 2023-09-01
      endDate: 2024-05-01
      highlights:
        - Developed an efficient method for encoding 4k and 8k video at real-time speeds. 
        - Utilized the FFmpeg library and C++ for the encoding operations.
        - Streamed test video to YouTube.
        - Began exploring GStreamer as an alternative to enhance performance further.

  - organization: Stevens Institute of Technology
    url: https://www.stevens.edu
    location: Hoboken, New Jersey
    positions:
    - position: MakerSpace Student Assistant
      startDate: 2023-08-23
      endDate: 2024-05-01
      highlights:
        - Designed and architected a system to automate the process of granting users access to equipment.
        - Performed API requests between the university Canvas LMS instance and the Grit Automation system.
        - Processed quiz submission entries with a dataframe into a final CSV format.
        - Implemented multiple microservices as other small utilities.
        - Packaged everything together using the Nix package manager and Docker images.

  - organization: G3 Technologies, Inc.
    url: https://www.g3ti.net/
    location: New Providence, New Jersey
    positions:
    - position: System Integration and Test Engineer Co-Op
      startDate: 2022-05-25
      endDate: 2022-12-13
      highlights:
        - Performed hardware checkout testing on units to be sent to customers.
        - Tested new software releases for various products.
        - Automated testing procedures through the use of Bash and Python scripts.
        - Parallelized existing scripts to test multiple simultaneous devices at once.

  - organization: NeuraFlash
    url: https://www.neuraflash.com/
    location: Remote
    positions:
    - position: Software Development Co-Op
      startDate: 2021-08-01
      endDate: 2021-12-01
      highlights:
        - Architected and implemented a service in Python to improve the time-sheet submission process.
        - Used Salesforce's OAuth 2.0 flow to allow users to log in with their company accounts.
        - Performed API calls between Slack and Salesforce to facilitate the creation and submission of time-sheets.

education:
  - institution: Stevens Institute of Technology
    url: https://www.stevens.edu/
    area: Electrical Engineering
    studyType: Bachelor of Engineering
    startDate: 2019-08-26
    endDate: 2024-05-22
    location: Hoboken, New Jersey
    honors:
      - 3.969 / 4.0 GPA
      - Dean's List
    courses:
      - Data Structures & Algorithms
      - Digital Signal Processing
      - Modeling & Simulation
      - Microprocessor Systems
    highlights: []

affiliations:
  - organization: Stevens IEEE Student Chapter
    location: Hoboken, New Jersey
    url:
    startDate: 2022-09-01
    endDate: 2023-05-01
    position: Co-Communications Chair
    highlights: 
      - Communicated with the student body about upcoming events.
      - Assisted with the running of weekly events.

  - organization: Stevens IEEE Student Chapter
    location: Hoboken, New Jersey
    url:
    startDate: 2023-09-01
    endDate: 2024-05-01
    position: Vice President
    highlights: 
      - Created and lead weekly events for the student body.
      - Assisted the president and elected board with various duties.

  - organization: Stevens Poker Club
    position: Secretary
    location: Hoboken, New Jersey
    url:
    startDate: 2021-09-01
    endDate: 2022-05-01
    highlights:
      - Performed weekly announcements and assisted with weekly tournaments.

  - organization: Stevens Poker Club
    position: Vice President
    location: Hoboken, New Jersey
    url:
    startDate: 2022-09-01
    endDate: 2023-05-01
    highlights:
      - Led weekly tournaments and taught new players basic strategy.

awards: 
certificates:
publications:

projects:
  - name: "C.A.T.S: Camera Assisted Tracking System"
    url: https://github.com/dkrautha/CATS-ECE
    affiliation: Stevens Institute of Technology
    startDate: 2023-09-01
    endDate: 2024-04-26
    highlights:
      - Trained an image detection model based on MobileNetV2 with TensorFlow Lite. 
      - Identifies dozens of breeds of cats and dogs, as well as other similar animals.
      - Quantized the model for use with Google Coral devices.

  - name: "XBF: Xtensible Binary Format"
    url: https://github.com/XtensibleBinaryFormat/XBF
    affiliation: Stevens Institute of Technology
    startDate: 2023-05-23
    endDate: 2023-08-19
    highlights:
      - Designed a novel self-describing, binary data interchange format specification.
      - Intended as a replacement and competitor to JSON and MessagePack.
      - Created a reference implementation of the specification in Rust.
      - Demonstrated meaningful size and speed improvements over similar format.
      - Authored an IEEE formatted research paper.

  - name: "GrailGUI: Native GUI Toolkit Research"
    url: https://github.com/StevensDeptECE/GrailGUI
    affiliation: Stevens Institute of Technology
    startDate: 2021-06-10
    endDate: 2021-08-25
    highlights:
      - Implemented audio and video playback features with the open source MPV library.
      - Wrote direct OpenGL code and shaders for displaying images and other content.
      - Developed graph components such as line, scatter, and bar graphs. 
      - Created a standalone statistics library to support the graph components.

skills:
  - category: Soft Skills
    skills:
      - Public Speaking
      - Communication
      - Critical Thinking
      - Leadership
      - Technical Writing

  - category: Programming Languages
    skills:
      - Python
      - Rust
      - C++
      - C
      - Java
      - Bash
      - TypeScript

  - category: Specific Technologies
    skills:
    - TensorFlow
    - Flask
    - Pydantic
    - Polars

  - category: Markup Languages
    skills:
      - Markdown
      - Typst
      - LaTeX
      - HTML
      - CSS

  - category: Tools
    skills:
      - Git
      - Docker
      - Podman
      - Gdb
      - Meson
      - CMake
      - Microsoft Office Suite

  - category: Operating Systems
    skills:
      - NixOS
      - Ubuntu
      - Windows 10
      - Debian
      - Fedora
      - OpenSUSE
      - Arch Linux

references:
#   # You can comment out the entries below if you don't have any references
#   # The same goes for the publications
#
#   - name: Dr. Jane Austin
#     reference: John was a great student. He was always eager to learn new things and was very passionate about his studies. As his mentor, I am proud to say that he was also a great leader and was able to lead his team to victory in the 2020 SCG Bangkok Business Challenge Global Competition.
#     url: https://janeaustin.com
